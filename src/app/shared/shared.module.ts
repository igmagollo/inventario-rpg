import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { BrMoneyPipe } from './pipes/br-money.pipe';

@NgModule({
  declarations: [BrMoneyPipe],
  imports: [
    CommonModule,
  ],
  exports: [
    BrMoneyPipe
  ],
  providers: [
    BrMoneyPipe
  ]
})
export class SharedModule {
}
