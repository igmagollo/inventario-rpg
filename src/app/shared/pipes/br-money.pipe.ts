import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'brMoney'
})
export class BrMoneyPipe implements PipeTransform {

  transform(value: number): string {
    let number = value.toFixed(2);
    number = '$' + number;
    return number;
  }

}
