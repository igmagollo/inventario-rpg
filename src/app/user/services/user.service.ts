import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import * as config from '../../shared/config';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private _isLoggedIn = false;

  constructor(private http: HttpClient) {
  }

  login(username: string, password: string) {
    return this.http.post(config.api + 'login', {username, password},  { responseType: 'json' });
  }

  setLoggedIn() {
    this._isLoggedIn = true;
  }

  get isLoggedIn(): boolean {
    return this._isLoggedIn;
  }
}
