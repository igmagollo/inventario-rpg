import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Item} from '../item';

@Component({
  selector: 'iv-inventario',
  templateUrl: './inventario.component.html',
  styleUrls: ['./inventario.component.scss']
})
export class InventarioComponent implements OnInit, OnChanges {
  @Input() items: Item[] = [];
  private _value = 0;
  @Output() selectedItem = new EventEmitter<Item>();
  @Output() valor = new EventEmitter<number>();

  constructor() {
  }

  ngOnInit() {
    this.items.push(
      {
        titulo: 'Vestido Alada Cor de Rosa',
        descricao: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
        quantidade: 1,
        data_aquisicao: '31/01/2019',
        valor: 13
      });
    this.items.push(
      {
        titulo: 'Vestido',
        descricao: 'stealth',
        quantidade: 1,
        data_aquisicao: '31/01/2019',
        valor: 13
      });
    this.items.push(
      {
        titulo: 'Vestido',
        descricao: 'stealth',
        quantidade: 1,
        data_aquisicao: '31/01/2019',
        valor: 13
      });
    this.items.push(
      {
        titulo: 'Vestido',
        descricao: 'stealth',
        quantidade: 1,
        data_aquisicao: '31/01/2019',
        valor: 13
      });
    this.items.forEach(item => this._value += item.valor);
    this.valor.emit(this._value);
  }

  onSelectItem(item: Item) {
    this.selectedItem.emit(item);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.items && !changes.items.isFirstChange()) {
      this.items = changes.items.currentValue;
      this._value = 0;
      this.items.forEach(item => this._value += item.valor);
      this.valor.emit(this._value);
    }
  }


}
