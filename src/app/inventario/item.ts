export interface Item {
  titulo: string;
  descricao: string;
  quantidade: number;
  valor: number;
  data_aquisicao: string;
}
