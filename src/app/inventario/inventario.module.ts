import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ItemComponent} from './item/item.component';
import {InventarioComponent} from './inventario/inventario.component';
import {ItemDescricaoComponent} from './item-descricao/item-descricao.component';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  declarations: [
    ItemComponent,
    InventarioComponent,
    ItemDescricaoComponent],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    InventarioComponent,
    ItemDescricaoComponent
  ]
})
export class InventarioModule {
}
