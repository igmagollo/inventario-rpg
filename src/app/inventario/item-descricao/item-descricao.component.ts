import {Component, Input, OnInit} from '@angular/core';
import {Item} from '../item';

@Component({
  selector: 'iv-item-descricao',
  templateUrl: './item-descricao.component.html',
  styleUrls: ['./item-descricao.component.scss']
})
export class ItemDescricaoComponent implements OnInit {
  @Input() item: Item;
  constructor() { }

  ngOnInit() {
  }

}
