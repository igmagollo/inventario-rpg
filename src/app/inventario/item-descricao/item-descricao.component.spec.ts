import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemDescricaoComponent } from './item-descricao.component';

describe('ItemDescricaoComponent', () => {
  let component: ItemDescricaoComponent;
  let fixture: ComponentFixture<ItemDescricaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemDescricaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemDescricaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
