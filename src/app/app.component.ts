import {Component} from '@angular/core';
import {Item} from './inventario/item';

@Component({
  selector: 'iv-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  selectedItem: Item;
  valor = 0;
  onSelectItem(item: Item) {
    this.selectedItem = item;
  }
  onChangeValor(val: number) {
    this.valor = val;
  }
}
